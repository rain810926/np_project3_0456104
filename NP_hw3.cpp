#include <windows.h>
#include <list>
#include <string.h>
#include <stdlib.h>
using namespace std;

#include "resource.h"

#define SERVER_PORT 7799
#define BUFLEN 10000
#define WM_SOCKET_NOTIFY (WM_USER + 1)
#define MAXFORM 5
#define MAXCMD 15000

typedef struct client
{
	char hostIP[30];
	int port;
	char fileName[30];
	int sockfd;
	int filefd;
	FILE *fd;
	char terminalID[3];
	char cmdBuf[MAXCMD];
	char printCmd[MAXCMD];
} client;

int websock, connect_cnt;
int wNeed[MAXFORM];
bool status[MAXFORM] = { 0 }, unsend[MAXFORM] = { 0 };
client cli_table[MAXFORM];

int connect(client *cli);
void response(int, HWND hwnd, int);
void get_query(char *get, HWND hwndEdit);
int recv_msg(client *cli, bool *status);
char *str_replace(char *source, char *find, char *replace);

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf(HWND, TCHAR *, ...);
//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{

	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;

	int err;


	switch (Message)
	{
	case WM_INITDIALOG:
		hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_LISTEN:

			WSAStartup(MAKEWORD(2, 0), &wsaData);

			//create master socket
			msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

			if (msock == INVALID_SOCKET) {
				EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
				WSACleanup();
				return TRUE;
			}

			err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT);

			if (err == SOCKET_ERROR) {
				EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
				closesocket(msock);
				WSACleanup();
				return TRUE;
			}

			//fill the address info about server
			sa.sin_family = AF_INET;
			sa.sin_port = htons(SERVER_PORT);
			sa.sin_addr.s_addr = INADDR_ANY;

			//bind socket
			err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

			if (err == SOCKET_ERROR) {
				EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
				WSACleanup();
				return FALSE;
			}

			err = listen(msock, 2);

			if (err == SOCKET_ERROR) {
				EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
				WSACleanup();
				return FALSE;
			}
			else {
				EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
			}

			break;
		case ID_EXIT:
			EndDialog(hwnd, 0);
			break;
		};
		break;

	case WM_CLOSE:
		EndDialog(hwnd, 0);
		break;

	case WM_SOCKET_NOTIFY:
		switch (WSAGETSELECTEVENT(lParam))

		{
		case FD_ACCEPT: {
			int n, i;
			char get[BUFLEN];
			ssock = accept(msock, NULL, NULL);
			//WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY, FD_CLOSE);
			Socks.push_back(ssock);
			EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());
			if ((n = recv(ssock, get, BUFLEN, 0)) > 0);
			EditPrintf(hwndEdit, TEXT("%s\r\n"), get);
			if (strncmp(get, "GET", 3) == 0) {
				if (strncmp(get + 5, "form_get.html", strlen("form_get.html")) == 0) {
					response(ssock, hwnd, 0);
					closesocket(ssock);
				}
				if (strncmp(get + 5, "hw3.cgi", strlen("hw3.cgi")) == 0) {
					get_query(get, hwndEdit);
					response(ssock, hwnd, 1);
					websock = ssock;
				}
			}
			break; }
		case FD_READ: {
			//Write your code for read event here.
			int i, n;
			for (i = 0; i < MAXFORM; i++) {
				if (cli_table[i].sockfd == wParam)
					break;
			}
			EditPrintf(hwndEdit, TEXT("=== READ(%d)===\r\n"), i);
			if ((n = recv_msg(&cli_table[i], &status[i])) == 0) {
				closesocket(cli_table[i].sockfd);
				connect_cnt--;
			}

			if (status[i]) {
				EditPrintf(hwndEdit, TEXT("=== R TO W(%d)===\r\n"), i);
				WSAAsyncSelect(cli_table[i].sockfd, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
			}

			if (cli_table[i].fd == NULL) {
				EditPrintf(hwndEdit, TEXT("=== open file failed(%d)===\r\n"), i);
				break;
			}

			if (!unsend[i] ) {
				EditPrintf(hwndEdit, TEXT("=== read file(%d)===\r\n"), i);
				if (fgets(cli_table[i].cmdBuf, MAXCMD, cli_table[i].fd) == NULL) {
					fclose(cli_table[i].fd);
				} else if (n > 0) {
					unsend[i] = 1;
					wNeed[i] = strlen(cli_table[i].cmdBuf);
					strncpy(cli_table[i].printCmd, cli_table[i].cmdBuf, strlen(cli_table[i].cmdBuf));
					strtok(cli_table[i].printCmd, "\r\n");
					EditPrintf(hwndEdit, TEXT("=== %s ===\r\n"), cli_table[i].printCmd);
				}
			}
			
			if(connect_cnt == 0)
				closesocket(websock);

			break; }
		case FD_WRITE: {
			//Write your code for write event here
			int i, n;
			char msg[MAXCMD];
			for (i = 0; i < MAXFORM; i++) {
				if (cli_table[i].sockfd == wParam)
					break;
			}
			EditPrintf(hwndEdit, TEXT("=== WRITE(%d)===\r\n"), i);

			if (status[i] && unsend[i]) {
				//printf("writing<br/>");
				n = send(cli_table[i].sockfd, cli_table[i].cmdBuf, wNeed[i], 0);
				wNeed[i] -= n;
				strncpy(cli_table[i].cmdBuf, cli_table[i].cmdBuf + n, wNeed[i]);
				if (n <= 0 || wNeed[i] == 0) {
					//printf("write complete<br/>");
					EditPrintf(hwndEdit, TEXT("=== W TO R(%d)===\r\n"), i);
					unsend[i] = 0;
					status[i] = 0;
					WSAAsyncSelect(cli_table[i].sockfd, hwnd, WM_SOCKET_NOTIFY, FD_READ);
				}
				if (wNeed[i] == 0) {
					sprintf(msg, "<script>document.all['%s'].innerHTML += \"%s<br/>\";</script>\n\0", cli_table[i].terminalID, cli_table[i].printCmd);
					msg[strlen(msg)] = 0;
					send(websock, msg, strlen(msg), 0);
				}

			}
		

			break; }
		case FD_CLOSE: {
			break; }
		};
		break;

	default:
		return FALSE;


	};

	return TRUE;
}

int EditPrintf(HWND hwndEdit, TCHAR * szFormat, ...)
{
	TCHAR   szBuffer[15000];
	va_list pArgList;

	va_start(pArgList, szFormat);
	wvsprintf(szBuffer, szFormat, pArgList);
	va_end(pArgList);

	SendMessage(hwndEdit, EM_SETSEL, (WPARAM)-1, (LPARAM)-1);
	SendMessage(hwndEdit, EM_REPLACESEL, FALSE, (LPARAM)szBuffer);
	SendMessage(hwndEdit, EM_SCROLLCARET, 0, 0);
	return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0);
}

void get_query(char *get, HWND hwndEdit)
{
	char *query, *tmp;
	int i;
	query = strtok(get, " /?");
	query = strtok(NULL, " /?");
	query = strtok(NULL, " /?");
	EditPrintf(hwndEdit, TEXT("%s\r\n"), query);
	tmp = strtok(query, "&");
	for (i = 0; i < MAXFORM; i++) {
		strcpy(cli_table[i].hostIP, tmp + 3);
		tmp = strtok(NULL, "&");
		cli_table[i].port = atoi(tmp + 3);
		tmp = strtok(NULL, "&");
		strcpy(cli_table[i].fileName, tmp + 3);
		tmp = strtok(NULL, "&");
	}
	for (i = 0; i < MAXFORM; i++)
		EditPrintf(hwndEdit, TEXT("%s %d %s\r\n"), cli_table[i].hostIP, cli_table[i].port, cli_table[i].fileName);

}

void response(int clifd, HWND hwnd, int type)
{
	char buffer[BUFLEN];
	switch (type) {
	case 0: {
		FILE *fd = fopen("form_get.html", "r");
		if (fd != NULL) {
			send(clifd, "HTTP/1.0 200 OK\n\n", strlen("HTTP/1.0 200 OK\n\n"), 0);
			while (fgets(buffer, BUFLEN, fd) != NULL) {
				send(clifd, buffer, strlen(buffer), 0);
			}
			fclose(fd);
		}
		else send(clifd, "HTTP/1.0 404 Not Found\n", strlen("HTTP/1.0 404 Not Found\n"), 0); //FILE NOT FOUND
		break; }
	case 1: {
		int i;
		connect_cnt = 0;
		char msg[BUFLEN];
		send(clifd, "HTTP/1.0 200 OK\n\n", strlen("HTTP/1.0 200 OK\n\n"), 0);
		send(clifd, "<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n", strlen("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n"), 0);
		send(clifd, "<title>Network Programming Homework 3</title>\n</head>\n<body bgcolor=#336699>\n", strlen("<title>Network Programming Homework 3</title>\n</head>\n<body bgcolor=#336699>\n"), 0);
		send(clifd, "<font face=\"Courier New\" size=2 color=#FFFF99>\n<table width=\"800\" border=\"1\">\n<tr>\n", strlen("<font face=\"Courier New\" size=2 color=#FFFF99>\n<table width=\"800\" border=\"1\">\n<tr>\n"), 0);
		for (i = 0; i<MAXFORM; i++) {
			if (cli_table[i].port) {
				if (connect(&cli_table[i]))
					WSAAsyncSelect(cli_table[i].sockfd, hwnd, WM_SOCKET_NOTIFY, FD_READ);
				sprintf(cli_table[i].terminalID, "m%d", connect_cnt);
				connect_cnt++;
				sprintf(msg, "<td>%s</td>", cli_table[i].hostIP);
				send(clifd, msg, strlen(msg), 0);
			}
		}
		send(clifd, "\n<tr>\n", strlen("\n<tr>\n"), 0);
		for (i = 0; i<MAXFORM; i++) {
			if (cli_table[i].port) {
				sprintf(msg, "<td valign=\"top\" id=\"%s\"></td>", cli_table[i].terminalID);
				send(clifd, msg, strlen(msg), 0);
			}
		}
		send(clifd, "</tr>\n</table>\n", strlen("</tr>\n</table>\n"), 0);

		break; }
	}
	return;
}

int connect(client *cli)
{
	//handle socket
	struct sockaddr_in client_sin;
	struct hostent *he;
	if ((he = gethostbyname(cli->hostIP)) == NULL) exit(1);
	cli->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	memset(&client_sin, 0, sizeof(client_sin));
	client_sin.sin_family = AF_INET;
	client_sin.sin_addr = *((struct in_addr *)he->h_addr);
	client_sin.sin_port = htons(cli->port);
	if (connect(cli->sockfd, (struct sockaddr *)&client_sin, sizeof(client_sin)) < 0) {
		if (errno != EINPROGRESS) return 0;
	}
	//open file
	cli->fd = fopen(cli->fileName, "r");
	//cli->filefd = fileno(cli->fd);
	return 1;
}

int recv_msg(client *cli, bool *status)
{
	char buf[MAXCMD], msg[MAXCMD], *tmp, *result;
	int len, i;

	len = recv(cli->sockfd, buf, sizeof(buf) - 1, 0);
	if (len < 0) return -1;
	buf[len] = 0;
	if (len > 0)
	{
		result = str_replace(buf, "<", "&lt;");
		result = str_replace(result, ">", "&gt;");
		for (tmp = strtok(result, "\n\r"); tmp; tmp = strtok(NULL, "\n\r"))
		{
			if (strncmp(tmp, "% ", 2) == 0) {
				*status = 1;
				//printf("write unlock<br/>");
				sprintf(msg, "<script>document.all['%s'].innerHTML += \"%s\";</script>\n", cli->terminalID, tmp);
				send(websock, msg, strlen(msg), 0);
			}
			else {
				sprintf(msg, "<script>document.all['%s'].innerHTML += \"%s<br/>\";</script>\n", cli->terminalID, tmp);  // echo input
				send(websock, msg, strlen(msg), 0);
			}
		}
	}
	fflush(stdout);
	return len;
} //end recv_msg

char *str_replace(char *source, char *find, char *replace)
{
	int cur_len = strlen(source);
	int dif_len = strlen(replace) - strlen(find);
	char *result;
	result = (char *)malloc((cur_len + 1) * sizeof(char));
	strcpy(result, "");

	while (*source != '\0') {
		if (strncmp(source, find, strlen(find)) == 0) {
			cur_len += dif_len;
			result = (char *)realloc(result, (cur_len + 1) * sizeof(char));
			strcat(result, replace);
			source += strlen(find);
		}
		else {
			strncat(result, source, 1);
			source++;
		}
	}
	return result;
}
