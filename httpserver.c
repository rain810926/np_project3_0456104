#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>

#define BUFLEN 1024

int build_socket(void);
void response(int, char *, int);


int main(int argc, char* argv[])
{
  int sockfd = build_socket(), clifd, pid, n;
  struct sockaddr_in cli_addr;
	socklen_t clilen;
	clilen = sizeof(cli_addr);

  while (clifd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen)) {
    if( (pid = fork()) < 0)
      exit(1);
    else if (pid == 0) {
      close(sockfd);
      char GET[BUFLEN], *tmp, *afname;
      int n;
      read(clifd, GET, BUFLEN);
      tmp = strtok(GET, " /?");
      tmp = strtok(NULL, " /?");
      printf("%s\n", tmp);

      afname = strchr(tmp, '.');
      if(strcmp(afname+1, "html") == 0)
        response(clifd, tmp, 0);
      else if(strcmp(afname+1, "cgi") == 0)
        response(clifd, tmp, 1);
      close(clifd);
      return 0;
    } //end child
    close(clifd);
  } //end while
}

int build_socket(void)
{
	int sockfd, port;
  int binding = 0;
	struct sockaddr_in serv_addr;


  sockfd =  socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
      printf("when opening socket... \n");
	bzero ((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;

  do {
    if(binding) printf("when binding... \n");
  	printf("Enter port: ");
  	scanf("%d", &port);
    serv_addr.sin_port = htons(port);
  } while( binding = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0);

    listen(sockfd,5);

    return sockfd;
}

void response(int clifd, char *fname, int type)
{
  char buffer[BUFLEN];
  switch(type){
    case 0: {
      FILE *fd = fopen(fname, "r");
      if(fd != NULL) {
        send(clifd, "HTTP/1.0 200 OK\n\n", strlen("HTTP/1.0 200 OK\n\n"), 0);
        dup2(clifd, STDOUT_FILENO);
        while(fgets(buffer, BUFLEN, fd) != NULL) {
          printf("%s", buffer);
        }
        fclose(fd);
      } else send(clifd, "HTTP/1.0 404 Not Found\n", strlen("HTTP/1.0 404 Not Found\n"), 0); //FILE NOT FOUND
      break; }
    case 1: {
      send(clifd, "HTTP/1.0 200 OK\n", strlen("HTTP/1.0 200 OK\n"), 0);

      sprintf(buffer, "./%s", fname);
      fname = strtok(NULL, " /?");
      printf("%s\n", fname);
      setenv("QUERY_STRING", fname, 1);
      dup2(clifd, STDOUT_FILENO);
      execl(buffer, buffer, (char *)0);
      break; }
  }
  return;
}
