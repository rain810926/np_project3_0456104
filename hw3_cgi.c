#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <errno.h>

#define MAXFORM 5
#define MAXCMD 15000

typedef struct client
{
  char hostIP[30];
  int port;
  char fileName[30];
  int sockfd;
  int filefd;
  FILE *fd;
  char terminalID[3];
  char cmdBuf[MAXCMD];
  char printCmd[MAXCMD];
} client;

int readline(int fd,char *ptr);
int recv_msg(client *cli, bool *status);
int connect(client *cli);
char *str_replace(char *source, char *find, char *replace);

int main(int argc, char* argv[], char *envp[])
{
   printf("Content-type: text/html\n\n");
   char *query = getenv("QUERY_STRING");
   client cli_table[MAXFORM];
   char *tmp;
   //取得表單
   int i, connect_count = 0;
   tmp = strtok(query, "&");
   for (i = 0; i < MAXFORM; i++) {
     strcpy(cli_table[i].hostIP, tmp+3);
     tmp=strtok(NULL, "&");
     cli_table[i].port = atoi(tmp+3);
     tmp=strtok(NULL, "&");
     strcpy(cli_table[i].fileName, tmp+3);
     tmp=strtok(NULL, "&");
   }

  //  handle fds
   fd_set rfds, wfds, arfds, awfds;
   int nfds = 0;
   FD_ZERO(&rfds);
   FD_ZERO(&wfds);
   FD_ZERO(&arfds);
   FD_ZERO(&awfds);

   printf("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n"
        "<title>Network Programming Homework 3</title>\n</head>\n<body bgcolor=#336699>\n"
        "<font face=\"Courier New\" size=2 color=#FFFF99>\n<table width=\"800\" border=\"1\">\n<tr>\n");

   //handle every form
   for (i=0; i<MAXFORM; i++) {
     if(cli_table[i].port) {
       connect(&cli_table[i]);
       sprintf(cli_table[i].terminalID, "m%d", connect_count);
       connect_count++;
       printf("<td>%s</td>", cli_table[i].hostIP);
     }
   }
   printf("\n<tr>\n");
   for (i=0; i<MAXFORM; i++) {
     if(cli_table[i].port) {
       FD_SET(cli_table[i].filefd, &arfds);
       nfds = nfds > cli_table[i].filefd? nfds : cli_table[i].filefd+1;
       FD_SET(cli_table[i].sockfd, &arfds);
       nfds = nfds > cli_table[i].sockfd? nfds : cli_table[i].sockfd+1;
       printf("<td valign=\"top\" id=\"%s\"></td>", cli_table[i].terminalID);
     }
   }
   printf("</tr>\n</table>\n");


   bool status[MAXFORM] = {0}, unsend[MAXFORM] = {0}, isExit[MAXFORM] = {0};
   int wNeed[MAXFORM], n;
   while(connect_count) {
     memcpy(&rfds, &arfds, sizeof(rfds));
     memcpy(&wfds, &awfds, sizeof(wfds));
     if ( select(nfds, &rfds, &wfds, NULL, NULL) < 0 ) exit(1);

     for (i=0; i<MAXFORM ; i++) {
       if(!status[i] && FD_ISSET(cli_table[i].sockfd, &rfds)) {
         if( (n = recv_msg(&cli_table[i], &status[i])) == 0) {
           FD_CLR(cli_table[i].sockfd, &arfds);
           close(cli_table[i].sockfd);
           connect_count--;
         }
         if(status[i]) {
           FD_CLR(cli_table[i].sockfd, &arfds);
           FD_SET(cli_table[i].sockfd,&awfds);
         }
       }
     }//end for

    for (i=0; i<MAXFORM ; i++) {
      if(!unsend[i] && FD_ISSET(cli_table[i].filefd, &rfds)) {
        if(fgets(cli_table[i].cmdBuf, MAXCMD, cli_table[i].fd) == NULL) {
          FD_CLR(cli_table[i].filefd, &arfds);
          close(cli_table[i].filefd);
       } else {
         unsend[i] = 1;
         wNeed[i] = strlen(cli_table[i].cmdBuf);
         strncpy(cli_table[i].printCmd, cli_table[i].cmdBuf, strlen(cli_table[i].cmdBuf));
         strtok(cli_table[i].printCmd, "\r\n");
       }
      }
    }//end for

     for (i=0; i<MAXFORM ; i++) {
       if(status[i] && FD_ISSET(cli_table[i].sockfd, &wfds) && unsend[i]) {
         //printf("writing<br/>");
         n = write(cli_table[i].sockfd, cli_table[i].cmdBuf, wNeed[i]);
         wNeed[i] -= n;
         strncpy(cli_table[i].cmdBuf, cli_table[i].cmdBuf + n, wNeed[i]);
         if (n <= 0 || wNeed[i] == 0) {
           //printf("write complete<br/>");
           FD_CLR(cli_table[i].sockfd, &awfds);
           FD_SET(cli_table[i].sockfd,&arfds);
           unsend[i] = 0;
           status[i] = 0;
         }
         if(wNeed[i] == 0)
           printf("<script>document.all['%s'].innerHTML += \"%s<br/>\";</script>\n", cli_table[i].terminalID, cli_table[i].printCmd);

       }
     }//end for
   } //end while

} //end main

int recv_msg(client *cli, bool *status)
{
  char buf[MAXCMD], *tmp, *result;
  int len,i;

  len = read(cli->sockfd, buf, sizeof(buf)-1);
  if(len < 0) return -1;
  buf[len] = 0;
  if(len > 0)
  {
    result = str_replace(buf, "<", "&lt;");
    result = str_replace(result, ">", "&gt;");
    for(tmp=strtok(result,"\n\r"); tmp; tmp=strtok(NULL,"\n\r"))
    {
      if ( strncmp(tmp, "% ", 2) == 0) {
        *status = 1;
        //printf("write unlock<br/>");
        printf("<script>document.all['%s'].innerHTML += \"%s\";</script>\n", cli->terminalID, tmp);
     } else printf("<script>document.all['%s'].innerHTML += \"%s<br/>\";</script>\n", cli->terminalID, tmp);  // echo input
    }
  }
  fflush(stdout);
  return len;
} //end recv_msg

int connect(client *cli)
{
  //handle socket
  struct sockaddr_in client_sin;
  struct hostent *he;
  if((he=gethostbyname(cli->hostIP)) == NULL) exit(1);
  cli->sockfd = socket(AF_INET,SOCK_STREAM,0);
  memset(&client_sin, 0, sizeof(client_sin));
  client_sin.sin_family = AF_INET;
  client_sin.sin_addr = *((struct in_addr *)he->h_addr);
  client_sin.sin_port = htons(cli->port);
  if( connect(cli->sockfd, (struct sockaddr *)&client_sin, sizeof(client_sin)) < 0) {
    if (errno != EINPROGRESS) return 0;
  }
  //non-blocking
  int flag = fcntl(cli->sockfd,F_GETFL, 0);
  fcntl(cli->sockfd, F_SETFL, flag | O_NONBLOCK);
  //open file
  cli->fd = fopen(cli->fileName, "r");
  cli->filefd = fileno(cli->fd);
  return 1;
}

char *str_replace(char *source, char *find, char *replace)
{
  int cur_len = strlen(source);
  int dif_len = strlen(replace) - strlen(find);
  char *result;
  result = (char *)malloc( (cur_len + 1) * sizeof(char));
  strcpy(result, "");

  while(*source != '\0') {
    if(strncmp(source, find, strlen(find)) == 0) {
      cur_len += dif_len;
      result = (char *)realloc(result, (cur_len + 1) * sizeof(char));
      strcat(result, replace);
      source += strlen(find);
    } else {
      strncat(result, source, 1);
      source ++;
    }
  }
  return result;
}
